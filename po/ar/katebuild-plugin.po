# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Safa Alfulaij <safa1996alfulaij@gmail.com>, 2016.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-18 02:00+0000\n"
"PO-Revision-Date: 2016-09-03 11:14+0300\n"
"Last-Translator: Safa Alfulaij <safa1996alfulaij@gmail.com>\n"
"Language-Team: Arabic <doc@arabeyes.org>\n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 "
"&& n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"X-Generator: Lokalize 2.0\n"

#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr ""

#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, errs)
#: build.ui:36
#, kde-format
msgid "Output"
msgstr "الخرج"

#. i18n: ectx: property (text), widget (QPushButton, buildAgainButton)
#: build.ui:56
#, kde-format
msgid "Build again"
msgstr "ابنِ ثانية"

#. i18n: ectx: property (text), widget (QPushButton, cancelBuildButton)
#: build.ui:63
#, kde-format
msgid "Cancel"
msgstr "ألغِ"

#: buildconfig.cpp:26
#, kde-format
msgid "Add errors and warnings to Diagnostics"
msgstr ""

#: buildconfig.cpp:37
#, fuzzy, kde-format
#| msgid "Build again"
msgid "Build & Run"
msgstr "ابنِ ثانية"

#: buildconfig.cpp:43
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build & Run Settings"
msgstr "ابنِ الهدف المحدّد"

#: plugin_katebuild.cpp:212 plugin_katebuild.cpp:219 plugin_katebuild.cpp:1221
#, kde-format
msgid "Build"
msgstr "البناء"

#: plugin_katebuild.cpp:222
#, kde-format
msgid "Select Target..."
msgstr "اختر الهدف..."

#: plugin_katebuild.cpp:227
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build Selected Target"
msgstr "ابنِ الهدف المحدّد"

#: plugin_katebuild.cpp:232
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build and Run Selected Target"
msgstr "ابنِ الهدف المحدّد"

#: plugin_katebuild.cpp:237
#, kde-format
msgid "Stop"
msgstr "أوقف"

#: plugin_katebuild.cpp:242
#, kde-format
msgctxt "Left is also left in RTL mode"
msgid "Focus Next Tab to the Left"
msgstr ""

#: plugin_katebuild.cpp:262
#, kde-format
msgctxt "Right is right also in RTL mode"
msgid "Focus Next Tab to the Right"
msgstr ""

#: plugin_katebuild.cpp:284
#, kde-format
msgctxt "Tab label"
msgid "Target Settings"
msgstr "إعدادات الهدف"

#: plugin_katebuild.cpp:403
#, fuzzy, kde-format
#| msgid "Build again"
msgid "Build Information"
msgstr "ابنِ ثانية"

#: plugin_katebuild.cpp:619
#, kde-format
msgid "There is no file or directory specified for building."
msgstr "لا ملفّ أو دليل محدّد للبناء."

#: plugin_katebuild.cpp:623
#, kde-format
msgid ""
"The file \"%1\" is not a local file. Non-local files cannot be compiled."
msgstr "الملفّ \"%1\" ليس ملفًّا محليًّا. لا يمكن تصريف الملفّات غير المحليّة."

#: plugin_katebuild.cpp:670
#, kde-format
msgid ""
"Cannot run command: %1\n"
"Work path does not exist: %2"
msgstr ""

#: plugin_katebuild.cpp:684
#, kde-format
msgid "Failed to run \"%1\". exitStatus = %2"
msgstr "فشل تنفيذ \"%1\". حالة الخروج = %2"

#: plugin_katebuild.cpp:699
#, kde-format
msgid "Building <b>%1</b> cancelled"
msgstr "أُلغي بناء <b>%1</b>"

#: plugin_katebuild.cpp:806
#, kde-format
msgid "No target available for building."
msgstr "لا هدف متوفّر للبناء."

#: plugin_katebuild.cpp:820
#, kde-format
msgid "There is no local file or directory specified for building."
msgstr "لا ملفّ أو دليل محليّ محدّد للبناء."

#: plugin_katebuild.cpp:826
#, kde-format
msgid "Already building..."
msgstr "يبني بالفعل..."

#: plugin_katebuild.cpp:853
#, kde-format
msgid "Building target <b>%1</b> ..."
msgstr "يبني الهدف <b>%1</b>..."

#: plugin_katebuild.cpp:867
#, kde-kuit-format
msgctxt "@info"
msgid "<title>Make Results:</title><nl/>%1"
msgstr "<title>نتائج الصناعة/Make:</title><nl/>%1"

#: plugin_katebuild.cpp:903
#, kde-format
msgid "Build <b>%1</b> completed. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:909
#, kde-format
msgid "Found one error."
msgid_plural "Found %1 errors."
msgstr[0] "لم توجد أخطاء."
msgstr[1] "وُجد خطأ واحد."
msgstr[2] "وُجد خطآن."
msgstr[3] "وُجدت %1 أخطاء."
msgstr[4] "وُجد %1 خطأ."
msgstr[5] "وُجد %1 خطأ."

#: plugin_katebuild.cpp:913
#, kde-format
msgid "Found one warning."
msgid_plural "Found %1 warnings."
msgstr[0] "لم توجد تحذيرات."
msgstr[1] "وُجد تحذير واحد."
msgstr[2] "وُجد تحذيرين."
msgstr[3] "وُجدت %1 تحذيرات."
msgstr[4] "وُجد %1 تحذيرًا."
msgstr[5] "وُجد %1 تحذير."

#: plugin_katebuild.cpp:916
#, fuzzy, kde-format
#| msgid "Found one error."
#| msgid_plural "Found %1 errors."
msgid "Found one note."
msgid_plural "Found %1 notes."
msgstr[0] "لم توجد أخطاء."
msgstr[1] "وُجد خطأ واحد."
msgstr[2] "وُجد خطآن."
msgstr[3] "وُجدت %1 أخطاء."
msgstr[4] "وُجد %1 خطأ."
msgstr[5] "وُجد %1 خطأ."

#: plugin_katebuild.cpp:921
#, kde-format
msgid "Build failed."
msgstr "فشل البناء."

#: plugin_katebuild.cpp:923
#, kde-format
msgid "Build completed without problems."
msgstr "اكتمل البناء بلا مشاكل."

#: plugin_katebuild.cpp:928
#, kde-format
msgid "Build <b>%1 canceled</b>. %2 error(s), %3 warning(s), %4 note(s)"
msgstr ""

#: plugin_katebuild.cpp:952
#, kde-format
msgid "Cannot execute: %1 No working directory set."
msgstr ""

#: plugin_katebuild.cpp:1178
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark an error."
#| msgid "error"
msgctxt "The same word as 'gcc' uses for an error."
msgid "error"
msgstr "خطأ"

#: plugin_katebuild.cpp:1181
#, fuzzy, kde-format
#| msgctxt "The same word as 'make' uses to mark a warning."
#| msgid "warning"
msgctxt "The same word as 'gcc' uses for a warning."
msgid "warning"
msgstr "تحذير"

#: plugin_katebuild.cpp:1184
#, kde-format
msgctxt "The same words as 'gcc' uses for note or info."
msgid "note|info"
msgstr ""

#: plugin_katebuild.cpp:1187
#, kde-format
msgctxt "The same word as 'ld' uses to mark an ..."
msgid "undefined reference"
msgstr "مرجع غير معرّف"

#: plugin_katebuild.cpp:1220 TargetModel.cpp:285 TargetModel.cpp:297
#, kde-format
msgid "Target Set"
msgstr "مجموعة الأهداف"

#: plugin_katebuild.cpp:1222
#, kde-format
msgid "Clean"
msgstr "المسح"

#: plugin_katebuild.cpp:1223
#, kde-format
msgid "Config"
msgstr "الضّبط"

#: plugin_katebuild.cpp:1224
#, kde-format
msgid "ConfigClean"
msgstr "الضّبط والمسح"

#: plugin_katebuild.cpp:1415
#, kde-format
msgid "Cannot save build targets in: %1"
msgstr ""

#: TargetHtmlDelegate.cpp:50
#, kde-format
msgctxt "T as in Target set"
msgid "<B>T:</B> %1"
msgstr "<B>الهدف:</B> %1"

#: TargetHtmlDelegate.cpp:52
#, kde-format
msgctxt "D as in working Directory"
msgid "<B>Dir:</B> %1"
msgstr "<B>الدّليل:</B> %1"

#: TargetHtmlDelegate.cpp:101
#, fuzzy, kde-format
#| msgid "Leave empty to use the directory of the current document."
msgid ""
"Leave empty to use the directory of the current document.\n"
"Add search directories by adding paths separated by ';'"
msgstr "اتركه فارغًا لاستخدام دليل المستند الحاليّ."

#: TargetHtmlDelegate.cpp:105
#, kde-format
msgid ""
"Use:\n"
"\"%f\" for current file\n"
"\"%d\" for directory of current file\n"
"\"%n\" for current file name without suffix"
msgstr ""
"استخدم:\n"
"‏\"‎%f\" للملفّ الحاليّ\n"
"‏\"‎%d\" لدليل الملفّ الحاليّ\n"
"‏\"‎%n\" للملفّ الحاليّ بدون لاحقته"

#: TargetModel.cpp:530
#, kde-format
msgid "Project"
msgstr ""

#: TargetModel.cpp:530
#, kde-format
msgid "Session"
msgstr ""

#: TargetModel.cpp:624
#, kde-format
msgid "Command/Target-set Name"
msgstr "اسم الأمر/مجموعة الأهداف"

#: TargetModel.cpp:627
#, kde-format
msgid "Working Directory / Command"
msgstr "دليل العمل / الأمر"

#: TargetModel.cpp:630
#, kde-format
msgid "Run Command"
msgstr ""

#: targets.cpp:23
#, kde-format
msgid "Filter targets, use arrow keys to select, Enter to execute"
msgstr ""

#: targets.cpp:27
#, kde-format
msgid "Create new set of targets"
msgstr "أنشئ مجموعة أهداف جديدة"

#: targets.cpp:31
#, kde-format
msgid "Copy command or target set"
msgstr "انسخ الأمر أو مجموعة الأهداف"

#: targets.cpp:35
#, fuzzy, kde-format
#| msgid "Delete current set of targets"
msgid "Delete current target or current set of targets"
msgstr "احذف مجموعة الأهداف الحاليّة"

#: targets.cpp:40
#, kde-format
msgid "Add new target"
msgstr "أضف هدفًا جديدًا"

#: targets.cpp:44
#, kde-format
msgid "Build selected target"
msgstr "ابنِ الهدف المحدّد"

#: targets.cpp:48
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Build and run selected target"
msgstr "ابنِ الهدف المحدّد"

#: targets.cpp:52
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Move selected target up"
msgstr "ابنِ الهدف المحدّد"

#: targets.cpp:56
#, fuzzy, kde-format
#| msgid "Build selected target"
msgid "Move selected target down"
msgstr "ابنِ الهدف المحدّد"

#. i18n: ectx: Menu (Build Menubar)
#: ui.rc:6
#, kde-format
msgid "&Build"
msgstr "ا&بنِ"

#: UrlInserter.cpp:32
#, kde-format
msgid "Insert path"
msgstr "أدرج مسارًا"

#: UrlInserter.cpp:51
#, kde-format
msgid "Select directory to insert"
msgstr "اختر الدّليل لإدراجه"

#~ msgid "Project Plugin Targets"
#~ msgstr "أهداف ملحقة البناء"

#~ msgid "build"
#~ msgstr "البناء"

#~ msgid "clean"
#~ msgstr "المسح"

#~ msgid "quick"
#~ msgstr "السّريع"

#~ msgid "Building <b>%1</b> completed."
#~ msgstr "اكتمل بناء <b>%1</b>."

#~ msgid "Building <b>%1</b> had errors."
#~ msgstr "واجه بناء <b>%1</b> بعض الأخطاء."

#~ msgid "Building <b>%1</b> had warnings."
#~ msgstr "أُبلغت عن تحذيرات عند بناء <b>%1</b>."

#~ msgid "Show:"
#~ msgstr "أظهر:"

#~ msgctxt "Header for the file name column"
#~ msgid "File"
#~ msgstr "الملفّ"

#~ msgctxt "Header for the line number column"
#~ msgid "Line"
#~ msgstr "السّطر"

#~ msgctxt "Header for the error message column"
#~ msgid "Message"
#~ msgstr "الرّسالة"

#~ msgid "Next Error"
#~ msgstr "الخطأ التّالي"

#~ msgid "Previous Error"
#~ msgstr "الخطأ السّابق"

#, fuzzy
#~| msgctxt "The same word as 'make' uses to mark an error."
#~| msgid "error"
#~ msgid "Error"
#~ msgstr "خطأ"

#, fuzzy
#~| msgctxt "The same word as 'make' uses to mark a warning."
#~| msgid "warning"
#~ msgid "Warning"
#~ msgstr "تحذير"

#~ msgid "Only Errors"
#~ msgstr "الأخطاء فقط"

#~ msgid "Errors and Warnings"
#~ msgstr "الأخطاء والتّحذيرات"

#~ msgid "Parsed Output"
#~ msgstr "الخرج المحلّل"

#~ msgid "Full Output"
#~ msgstr "الخرج الكامل"

#~ msgid ""
#~ "Check the check-box to make the command the default for the target-set."
#~ msgstr "أشّر على مربّع التّأشير لجعل الأمر افتراضيًّا لمجموعة الأهداف."

#~ msgid "Select active target set"
#~ msgstr "اختر مجموعة الأهداف النّشطة"

#, fuzzy
#~| msgid "Build selected target"
#~ msgid "Filter targets"
#~ msgstr "ابنِ الهدف المحدّد"

#~ msgid "Build Default Target"
#~ msgstr "هدف البناء الافتراضيّ"

#, fuzzy
#~| msgid "Build Default Target"
#~ msgid "Build and Run Default Target"
#~ msgstr "هدف البناء الافتراضيّ"

#~ msgid "Build Previous Target"
#~ msgstr "هدف البناء السّابق"

#~ msgid "Active target-set:"
#~ msgstr "مجموعة الأهداف النّشطة:"

#~ msgid "config"
#~ msgstr "الضّبط"

#~ msgid "Kate Build Plugin"
#~ msgstr "ملحقة بناء ل‍«كيت»"

#~ msgid "Select build target"
#~ msgstr "اختر هدف البناء"

#~ msgid "Filter"
#~ msgstr "رشّح"

#~ msgid "Build Output"
#~ msgstr "خرج البناء"

#, fuzzy
#~| msgctxt "@info"
#~| msgid "<title>Make Results:</title><nl/>%1"
#~ msgctxt "@info"
#~ msgid "<title>Could not open file:</title><nl/>%1"
#~ msgstr "<title>نتائج الصناعة/Make:</title><nl/>%1"
