# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR This file is copyright:
# This file is distributed under the same license as the kate package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: kate\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-10-05 01:42+0000\n"
"PO-Revision-Date: 2021-09-19 13:41+0630\n"
"Last-Translator: \n"
"Language-Team: Burmese <kde-i18n-doc@kde.org>\n"
"Language: my\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Poedit 2.2.1\n"

#: gotosymboldialog.cpp:157 lspclientsymbolview.cpp:252
#, fuzzy, kde-format
msgid "Filter..."
msgstr "စစ်ထုတ်မည်..."

#. i18n: ectx: Menu (LSPClient Menubar)
#: lspclientconfigpage.cpp:98 lspclientconfigpage.cpp:103
#: lspclientpluginview.cpp:452 lspclientpluginview.cpp:607 ui.rc:6
#, kde-format
msgid "LSP Client"
msgstr "အယ်အက်စ်ပီ ပရိုဂရမ်"

#: lspclientconfigpage.cpp:213
#, fuzzy, kde-format
msgid "No JSON data to validate."
msgstr "\"stderr\" သို့ရေးခဲ့သော ဒေတာ -\n"

#: lspclientconfigpage.cpp:222
#, fuzzy, kde-format
msgid "JSON data is valid."
msgstr "ဂျီပီဂျီလက်မှတ်- မှန်ကန်သည့်လက်မှတ်ထိုးခဲ့သူ "

#: lspclientconfigpage.cpp:224
#, kde-format
msgid "JSON data is invalid: no JSON object"
msgstr ""

#: lspclientconfigpage.cpp:227
#, fuzzy, kde-format
msgid "JSON data is invalid: %1"
msgstr "မမှန်ကန်သောURL - %1"

#: lspclientconfigpage.cpp:275
#, kde-format
msgid "Delete selected entries"
msgstr ""

#: lspclientconfigpage.cpp:280
#, kde-format
msgid "Delete all entries"
msgstr ""

#: lspclientplugin.cpp:224
#, kde-format
msgid "LSP server start requested"
msgstr ""

#: lspclientplugin.cpp:227
#, kde-format
msgid ""
"Do you want the LSP server to be started?<br><br>The full command line is:"
"<br><br><b>%1</b><br><br>The choice can be altered via the config page of "
"the plugin."
msgstr ""

#: lspclientplugin.cpp:240
#, kde-format
msgid ""
"User permanently blocked start of: '%1'.\n"
"Use the config page of the plugin to undo this block."
msgstr ""

#: lspclientpluginview.cpp:419 lspclientpluginview.cpp:671
#, kde-format
msgid "LSP"
msgstr ""

#: lspclientpluginview.cpp:481
#, fuzzy, kde-format
msgid "Go to Definition"
msgstr "အဓိပ္ပါယ်ဖွင့်ဆိုချက်သို့ သွားမည်"

#: lspclientpluginview.cpp:483
#, fuzzy, kde-format
msgid "Go to Declaration"
msgstr "ကြေညာချက်သို့သွားမည်"

#: lspclientpluginview.cpp:485
#, fuzzy, kde-format
msgid "Go to Type Definition"
msgstr "အဓိပ္ပါယ်ဖွင့်ဆိုချက်သို့ သွားမည်"

#: lspclientpluginview.cpp:487
#, fuzzy, kde-format
msgid "Find References"
msgstr "ရှာဖွေမည်"

#: lspclientpluginview.cpp:490
#, fuzzy, kde-format
msgid "Find Implementations"
msgstr "အာပီအမ်-ဖိုင်း"

#: lspclientpluginview.cpp:492
#, fuzzy, kde-format
msgid "Highlight"
msgstr "မီးမောင်းထိုးပြရန်"

#: lspclientpluginview.cpp:494
#, kde-format
msgid "Symbol Info"
msgstr "သင်္ကေတ အချက်အလက်"

#: lspclientpluginview.cpp:496
#, fuzzy, kde-format
msgid "Search and Go to Symbol"
msgstr "အများသုံးသင်္ကေတ သို့သွားမည်"

#: lspclientpluginview.cpp:501
#, fuzzy, kde-format
msgid "Format"
msgstr "အချိုးအစား -"

#: lspclientpluginview.cpp:504
#, fuzzy, kde-format
msgid "Rename"
msgstr "အမည်ပြောင်းရန်"

#: lspclientpluginview.cpp:507
#, fuzzy, kde-format
msgid "Expand Selection"
msgstr "&Eတပ်ဘ်များဖြန့်မည်"

#: lspclientpluginview.cpp:510
#, kde-format
msgid "Shrink Selection"
msgstr ""

#: lspclientpluginview.cpp:513
#, fuzzy, kde-format
msgid "Switch Source Header"
msgstr "မူလဖိုင်လ်"

#: lspclientpluginview.cpp:516
#, fuzzy, kde-format
msgid "Expand Macro"
msgstr "&Eတပ်ဘ်များဖြန့်မည်"

#: lspclientpluginview.cpp:518
#, fuzzy, kde-format
msgid "Code Action"
msgstr "လုပ်ဆောင်ချက်မရှိ"

#: lspclientpluginview.cpp:533
#, kde-format
msgid "Show selected completion documentation"
msgstr ""

#: lspclientpluginview.cpp:536
#, kde-format
msgid "Enable signature help with auto completion"
msgstr ""

#: lspclientpluginview.cpp:539
#, fuzzy, kde-format
msgid "Include declaration in references"
msgstr "ကြေညာချက်သို့သွားမည် - %1"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplParens)
#: lspclientpluginview.cpp:542 lspconfigwidget.ui:96
#, kde-format
msgid "Add parentheses upon function completion"
msgstr ""

#: lspclientpluginview.cpp:545
#, fuzzy, kde-format
msgid "Show hover information"
msgstr "သတင်းအချက်အလက်"

#. i18n: ectx: property (text), widget (QCheckBox, chkOnTypeFormatting)
#: lspclientpluginview.cpp:548 lspconfigwidget.ui:47
#, fuzzy, kde-format
msgid "Format on typing"
msgstr "အချိုးအစား -"

#: lspclientpluginview.cpp:551
#, fuzzy, kde-format
msgid "Incremental document synchronization"
msgstr "မမှန်ကန်သော မှတ်တမ်းစာအုပ်"

#: lspclientpluginview.cpp:554
#, fuzzy, kde-format
msgid "Highlight goto location"
msgstr "သွားမည် - %1"

#: lspclientpluginview.cpp:563
#, kde-format
msgid "Show Inlay Hints"
msgstr ""

#: lspclientpluginview.cpp:567
#, fuzzy, kde-format
msgid "Show Diagnostics Notifications"
msgstr "ရှာဖွေဘားကို ပြရန်"

#: lspclientpluginview.cpp:572
#, fuzzy, kde-format
#| msgid "Show messages"
msgid "Show Messages"
msgstr "မှတ်စာများရှင်းမည်"

#: lspclientpluginview.cpp:577
#, fuzzy, kde-format
msgid "Server Memory Usage"
msgstr "မန်မိုရိကုန်ပြီ"

#: lspclientpluginview.cpp:581
#, kde-format
msgid "Close All Dynamic Reference Tabs"
msgstr ""

#: lspclientpluginview.cpp:583
#, kde-format
msgid "Restart LSP Server"
msgstr "အယ်အက်စ်ပီ ဆာဗာ ပြန်စတင်မည်"

#: lspclientpluginview.cpp:585
#, kde-format
msgid "Restart All LSP Servers"
msgstr ""

#: lspclientpluginview.cpp:596
#, kde-format
msgid "Go To"
msgstr ""

#: lspclientpluginview.cpp:623
#, fuzzy, kde-format
msgid "More options"
msgstr "ပိုမို"

#: lspclientpluginview.cpp:829 lspclientsymbolview.cpp:287
#, fuzzy, kde-format
msgid "Expand All"
msgstr "&Eတပ်ဘ်များဖြန့်မည်"

#: lspclientpluginview.cpp:830 lspclientsymbolview.cpp:288
#, fuzzy, kde-format
msgid "Collapse All"
msgstr "ပရောဂျက် အားလုံး"

#: lspclientpluginview.cpp:1033
#, kde-format
msgid "RangeHighLight"
msgstr ""

#: lspclientpluginview.cpp:1345
#, fuzzy, kde-format
msgid "Line: %1: "
msgstr "စာကြောင်း"

#: lspclientpluginview.cpp:1497 lspclientpluginview.cpp:1845
#: lspclientpluginview.cpp:1964
#, fuzzy, kde-format
msgid "No results"
msgstr "အက်စ်ကျူအယ် ရလဒ်များ"

#: lspclientpluginview.cpp:1556
#, fuzzy, kde-format
msgctxt "@title:tab"
msgid "Definition: %1"
msgstr "အဓိပ္ပါယ်ဖွင့်ဆိုချက်သို့ သွားမည် - %1"

#: lspclientpluginview.cpp:1562
#, fuzzy, kde-format
msgctxt "@title:tab"
msgid "Declaration: %1"
msgstr "ကြေညာချက်သို့သွားမည် - %1"

#: lspclientpluginview.cpp:1568
#, fuzzy, kde-format
msgctxt "@title:tab"
msgid "Type Definition: %1"
msgstr "အဓိပ္ပါယ်ဖွင့်ဆိုချက်သို့ သွားမည် - %1"

#: lspclientpluginview.cpp:1574
#, fuzzy, kde-format
msgctxt "@title:tab"
msgid "References: %1"
msgstr "%1 -"

#: lspclientpluginview.cpp:1586
#, fuzzy, kde-format
msgctxt "@title:tab"
msgid "Implementation: %1"
msgstr "မြင်ကွင်း ၁ အချိုး ၁"

#: lspclientpluginview.cpp:1599
#, fuzzy, kde-format
msgctxt "@title:tab"
msgid "Highlight: %1"
msgstr "မီးမောင်းထိုးပြရန်"

#: lspclientpluginview.cpp:1623 lspclientpluginview.cpp:1634
#: lspclientpluginview.cpp:1647
#, fuzzy, kde-format
msgid "No Actions"
msgstr "&Aလုပ်ဆောင်ချက်များ"

#: lspclientpluginview.cpp:1638
#, fuzzy, kde-format
msgid "Loading..."
msgstr "တွန်းတင်နေသည်..."

#: lspclientpluginview.cpp:1730
#, kde-format
msgid "No edits"
msgstr ""

#: lspclientpluginview.cpp:1804
#, fuzzy, kde-format
msgctxt "@title:window"
msgid "Rename"
msgstr "အမည်ပြောင်းရန်"

#: lspclientpluginview.cpp:1805
#, kde-format
msgctxt "@label:textbox"
msgid "New name (caution: not all references may be replaced)"
msgstr ""

#: lspclientpluginview.cpp:1851
#, fuzzy, kde-format
msgid "Not enough results"
msgstr "အက်စ်ကျူအယ် ရလဒ်များ"

#: lspclientpluginview.cpp:1920
#, kde-format
msgid "Corresponding Header/Source not found"
msgstr ""

#: lspclientpluginview.cpp:2116 lspclientpluginview.cpp:2153
#, fuzzy, kde-format
msgctxt "@info"
msgid "LSP Server"
msgstr ""
"ဆာဗာစနစ်အတွင်းပိုင်းအမှား\n"
"%1"

#: lspclientpluginview.cpp:2176
#, kde-format
msgctxt "@info"
msgid "LSP Client"
msgstr "အယ်အက်စ်ပီ ပရိုဂရမ်"

#: lspclientpluginview.cpp:2448
#, fuzzy, kde-format
#| msgid "Restart LSP Server"
msgid "Question from LSP server"
msgstr "အယ်အက်စ်ပီ ဆာဗာ ပြန်စတင်မည်"

#: lspclientservermanager.cpp:604
#, kde-format
msgid "Restarting"
msgstr "ပြန်စတင်နေသည်"

#: lspclientservermanager.cpp:604
#, fuzzy, kde-format
msgid "NOT Restarting"
msgstr ""
"သင်၏ လုပ်ငန်းဆောင်ရွက်မှုခန်းကို ပြန်စကြည့်ပါ သို့မဟုတ် ကေအိုင်အိုဒီမှတ်တမ်းများတွင် အမှားများ ရှာကြည့်ပါ။"

#: lspclientservermanager.cpp:605
#, kde-format
msgid "Server terminated unexpectedly ... %1 [%2] [homepage: %3] "
msgstr ""

#: lspclientservermanager.cpp:801
#, fuzzy, kde-format
msgid "Failed to find server binary: %1"
msgstr "'%1' ကို မစတင်နိုင်ခဲ့ပါ"

#: lspclientservermanager.cpp:804 lspclientservermanager.cpp:846
#, kde-format
msgid "Please check your PATH for the binary"
msgstr ""

#: lspclientservermanager.cpp:805 lspclientservermanager.cpp:847
#, kde-format
msgid "See also %1 for installation or details"
msgstr ""

#: lspclientservermanager.cpp:843
#, fuzzy, kde-format
msgid "Failed to start server: %1"
msgstr "'%1' ကို မစတင်နိုင်ခဲ့ပါ"

#: lspclientservermanager.cpp:851
#, fuzzy, kde-format
msgid "Started server %2: %1"
msgstr "%1၊ %2"

#: lspclientservermanager.cpp:886
#, kde-format
msgid "Failed to parse server configuration '%1': no JSON object"
msgstr ""

#: lspclientservermanager.cpp:889
#, kde-format
msgid "Failed to parse server configuration '%1': %2"
msgstr ""

#: lspclientservermanager.cpp:893
#, fuzzy, kde-format
msgid "Failed to read server configuration: %1"
msgstr ""
"ဆာဗာအား ယုံကြည်ရမှုစစ်ဆေးရာ ကျရှုံးခဲ့သည်(%1).\n"
"\n"

#: lspclientsymbolview.cpp:240
#, fuzzy, kde-format
msgid "Symbol Outline"
msgstr "ဒေသတွင်းသုံးသင်္ကေတ သို့သွားမည်"

#: lspclientsymbolview.cpp:278
#, fuzzy, kde-format
msgid "Tree Mode"
msgstr "သစ်ပင်ပုံစံမြင်ကွင်း"

#: lspclientsymbolview.cpp:280
#, fuzzy, kde-format
msgid "Automatically Expand Tree"
msgstr "သစ်ပင်ပုံစံမြင်ကွင်း"

#: lspclientsymbolview.cpp:282
#, kde-format
msgid "Sort Alphabetically"
msgstr "အက္ခရာစဥ်ဖြင့်စီမည်"

#: lspclientsymbolview.cpp:284
#, fuzzy, kde-format
msgid "Show Details"
msgstr "နောက်ကွတ်ကီး၏အသေးစိတ်ပြမည်"

#: lspclientsymbolview.cpp:445
#, kde-format
msgid "Symbols"
msgstr "သင်္ကေတများ"

#: lspclientsymbolview.cpp:576
#, fuzzy, kde-format
msgid "No LSP server for this document."
msgstr "ဆာဗာ ယုံကြည်မှုရယူခြင်း"

#. i18n: ectx: attribute (title), widget (QWidget, tab)
#: lspconfigwidget.ui:33
#, fuzzy, kde-format
msgid "Client Settings"
msgstr "&Sဆက်တင်"

#. i18n: ectx: property (text), widget (QCheckBox, chkFmtOnSave)
#: lspconfigwidget.ui:54
#, fuzzy, kde-format
msgid "Format on save"
msgstr "အချိုးအစား -"

#. i18n: ectx: property (text), widget (QCheckBox, chkSemanticHighlighting)
#: lspconfigwidget.ui:61
#, fuzzy, kde-format
msgid "Enable semantic highlighting"
msgstr "ဗွီအိပ်ချ်ဒီအယ် အရောင်တင်ပြခြင်း"

#. i18n: ectx: property (text), widget (QCheckBox, chkInlayHint)
#: lspconfigwidget.ui:68
#, kde-format
msgid "Enable inlay hints"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_2)
#: lspconfigwidget.ui:75
#, fuzzy, kde-format
msgid "Completions:"
msgstr "လုပ်ဆောင်ချက်မရှိ"

#. i18n: ectx: property (text), widget (QCheckBox, chkComplDoc)
#: lspconfigwidget.ui:82
#, kde-format
msgid "Show inline docs for selected completion"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSignatureHelp)
#: lspconfigwidget.ui:89
#, kde-format
msgid "Show function signature when typing a function call"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoImport)
#: lspconfigwidget.ui:103
#, kde-format
msgid "Add imports automatically if needed upon completion"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_5)
#: lspconfigwidget.ui:110
#, kde-format
msgid "Navigation:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkRefDeclaration)
#: lspconfigwidget.ui:117
#, kde-format
msgid "Count declarations when searching for references to a symbol"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkAutoHover)
#: lspconfigwidget.ui:124
#, kde-format
msgid "Show information about currently hovered symbol"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkHighlightGoto)
#: lspconfigwidget.ui:131
#, kde-format
msgid "Highlight target line when hopping to it"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_3)
#: lspconfigwidget.ui:138
#, fuzzy, kde-format
msgid "Server:"
msgstr ""
"ဆာဗာစနစ်အတွင်းပိုင်းအမှား\n"
"%1"

#. i18n: ectx: property (text), widget (QCheckBox, chkDiagnostics)
#: lspconfigwidget.ui:145
#, fuzzy, kde-format
msgid "Show program diagnostics"
msgstr "ရှာဖွေဘားကို ပြရန်"

#. i18n: ectx: property (text), widget (QCheckBox, chkMessages)
#: lspconfigwidget.ui:152
#, kde-format
msgid "Show notifications from the LSP server"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkIncrementalSync)
#: lspconfigwidget.ui:159
#, kde-format
msgid "Incrementally synchronize documents with the LSP server"
msgstr ""

#. i18n: ectx: property (text), widget (QLabel, label_6)
#: lspconfigwidget.ui:166
#, kde-format
msgid "Document outline:"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolSort)
#: lspconfigwidget.ui:173
#, fuzzy, kde-format
msgid "Sort symbols alphabetically"
msgstr "အမည်ဖြင့်စီမည်"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolDetails)
#: lspconfigwidget.ui:180
#, fuzzy, kde-format
msgid "Display additional details for symbols"
msgstr "အပိုင်းကန့်-ဝင်းဒိုး တွင် ပြမည်"

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolTree)
#: lspconfigwidget.ui:187
#, kde-format
msgid "Present symbols in a hierarchy instead of a flat list"
msgstr ""

#. i18n: ectx: property (text), widget (QCheckBox, chkSymbolExpand)
#: lspconfigwidget.ui:202
#, fuzzy, kde-format
msgid "Automatically expand tree"
msgstr "သစ်ပင်ပုံစံမြင်ကွင်း"

#. i18n: ectx: attribute (title), widget (QWidget, tab_4)
#: lspconfigwidget.ui:227
#, kde-format
msgid "Allowed && Blocked Servers"
msgstr ""

#. i18n: ectx: attribute (title), widget (QWidget, tab_2)
#: lspconfigwidget.ui:237
#, kde-format
msgid "User Server Settings"
msgstr "အသုံးပြုသူဆာဗာဆက်တင်"

#. i18n: ectx: property (text), widget (QLabel, label)
#: lspconfigwidget.ui:245
#, kde-format
msgid "Settings File:"
msgstr "ဆက်တင် ဖိုင်လ် -"

#. i18n: ectx: attribute (title), widget (QWidget, tab_3)
#: lspconfigwidget.ui:272
#, fuzzy, kde-format
msgid "Default Server Settings"
msgstr "ကိရိယာကို မူလသတ်မှတ်ချက်များသို့ ပြန်ပြောင်းမည်"

#. i18n: ectx: Menu (lspclient_more_options)
#: ui.rc:31
#, fuzzy, kde-format
msgid "More Options"
msgstr "ပိုမို"

#, fuzzy
#~ msgid "Quick Fix"
#~ msgstr "&Qအလျင်အမြန်ဖွင့်မည်"

#, fuzzy
#~ msgid "Show Diagnostics Highlights"
#~ msgstr "ရှာဖွေဘားကို ပြရန်"

#, fuzzy
#~ msgid "Show Diagnostics Marks"
#~ msgstr "ရှာဖွေဘားကို ပြရန်"

#, fuzzy
#~ msgid "Show Diagnostics on Hover"
#~ msgstr "ရှာဖွေဘားကို ပြရန်"

#, fuzzy
#~ msgid "Switch to Diagnostics Tab"
#~ msgstr "&Pရှေ့တပ်ဘ်"

#, fuzzy
#~ msgid "Error"
#~ msgstr "အမှား"

#~ msgid "Warning"
#~ msgstr "သတိ"

#, fuzzy
#~ msgid "Information"
#~ msgstr "သတင်းအချက်အလက်"

#, fuzzy
#~ msgid "Show diagnostics on hover"
#~ msgstr "ရှာဖွေဘားကို ပြရန်"

#, fuzzy
#~ msgid "General Options"
#~ msgstr "&Gယေဘုယျ"

#, fuzzy
#~ msgid "Add highlights"
#~ msgstr "ထည့်သွင်းမည်..."

#, fuzzy
#~ msgid "Add markers"
#~ msgstr "ထည့်သွင်းမည်..."

#, fuzzy
#~ msgid "Tree mode outline"
#~ msgstr "အသေးစိတ်ပြသစ်ပင်ပုံစံမြင်ကွင်း"
